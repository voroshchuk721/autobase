package com.yukonproject.autobase;

import com.yukonproject.autobase.model.Vehicle;
import com.yukonproject.autobase.model.enums.Condition;
import com.yukonproject.autobase.service.VehicleService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.text.ParseException;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AutoBaseApplication.class)
@ActiveProfiles("test")
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class VehicleControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() throws ParseException {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

        Vehicle vehicle = new Vehicle();
        vehicle.setId(0L);
        vehicle.setModel("Mercedes");
        vehicle.setPlateNumber("CE1021AA");
        vehicle.setCondition(Condition.READY);
        vehicle.setColor("Blue");
        vehicle.setPassengerAmount(8);
        vehicleService.create(vehicle);

    }

    @Test
    public void verifyAllVehiclesList() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/vehicle")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].model").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].serialNumber").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].condition").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].color").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].passengerAmount").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("[*].model").value("Mercedes"))
                .andExpect(MockMvcResultMatchers.jsonPath("[*].serialNumber").value("CE1021AA"))
                .andExpect(MockMvcResultMatchers.jsonPath("[*].condition").value(Condition.READY))
                .andExpect(MockMvcResultMatchers.jsonPath("[*].color").value("Blue"))
                .andExpect(MockMvcResultMatchers.jsonPath("[*].passengerAmount").value(8));
    }

    @Test
    public void verifyNotFoundVehicles() throws Exception {
        vehicleService.delete(1);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/vehicle")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void createVehicle() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/vehicle")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":\"1\", \"model\":\"BMW\", \"serialNumber\":\"AT2020CE\", \"condition\":\"READY\", \"color\":\"Brown\", \"passengerAmount\":\"20\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

    }

    @Test
    public void getVehicleById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/vehicle/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.model").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.serialNumber").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.condition").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.color").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.passengerAmount").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.model").value("Mercedes"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.serialNumber").value("CE1021AA"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.condition").value(Condition.READY))
                .andExpect(MockMvcResultMatchers.jsonPath("$.color").value("Blue"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.passengerAmount").value(8));
    }

    @Test
    public void getVehicleByIdNotFound() throws Exception {
        vehicleService.delete(1);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/vehicle/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateVehicle() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/vehicle/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":\"1\", \"model\":\"Volkswagen\", \"serialNumber\":\"AA2222CE\", \"condition\":\"READY\", \"color\":\"Black\", \"passengerAmount\":\"9\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateVehicleNotModified() throws Exception {
        vehicleService.delete(1);
        mockMvc.perform(MockMvcRequestBuilders
                .put("/vehicle/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":\"1\", \"model\":\"Volkswagen\", \"serialNumber\":\"AA2222CE\", \"condition\":\"READY\", \"color\":\"Black\", \"passengerAmount\":\"9\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotModified());
    }

    @Test
    public void deleteVehicle() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/vehicle/{id}", 1))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteVehicleNotModified() throws Exception {
        vehicleService.delete(1);
        mockMvc.perform(MockMvcRequestBuilders.delete("/vehicle/{id}", 1))
                .andExpect(status().isNotModified());
    }



}
