package com.yukonproject.autobase;

import com.yukonproject.autobase.model.Vehicle;
import com.yukonproject.autobase.model.enums.Condition;
import com.yukonproject.autobase.repository.VehicleRepository;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;


import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

//@DataJpaTest
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestDetails {

    @Autowired
    private VehicleRepository repository;

    @Test
    @Rollback(false)
    @Order(1)
    public void testCreateVehicle() {
        Vehicle vehicle = new Vehicle(4L, "Lanos",
                "AA1111AA", Condition.READY,
                "Black", 5);
        Vehicle savedVehicle = repository.save(vehicle);

        assertNotNull(savedVehicle);

    }

    @Test
    @Order(2)
    public void testFindVehicleByModelExist() {
        String model = "Lanos";
        Vehicle vehicle = repository.findByModel(model);

        assertThat(vehicle.getModel()).isEqualTo(model);
    }

    @Test
    @Order(3)
    public void testFindVehicleByModelNotExist() {
        String model = "Lanosss";
        Vehicle vehicle = repository.findByModel(model);

        assertNull(vehicle);
    }

    @Test
    @Rollback(false)
    @Order(5)
    public void testUpdateVehicle() {
        String vehicleModel = "Vazik";
        Vehicle vehicle = new Vehicle(1L, vehicleModel, "2121", Condition.READY, "Yellow", 10);
        vehicle.setId(1L);

        repository.save(vehicle);

        Vehicle updatedVehicle = repository.findByModel(vehicleModel);

        assertThat(updatedVehicle.getModel()).isEqualTo(vehicleModel);
    }

    @Test
    @Order(4)
    public void testListVehicle() {
        List<Vehicle> vehicles = repository.findAll();
        for (Vehicle vehicle : vehicles) {
            System.out.println(vehicle);
        }
        assertThat(vehicles).hasNoNullFieldsOrProperties();

        //assertThat(vehicle).size().isGraterThan(0);

    }

    @Test
    @Rollback(false)
    @Order(6)
    public void testDeleteVehicle() {
        Long id = 1L;
        boolean isExistBeforeDelete = repository.findById(id).isPresent();

        repository.deleteById(id);

        boolean notExistAfterDelete = repository.findById(id).isPresent();

        assertTrue(isExistBeforeDelete);
        assertFalse(notExistAfterDelete);

    }


}
