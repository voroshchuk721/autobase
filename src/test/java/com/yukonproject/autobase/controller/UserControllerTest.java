package com.yukonproject.autobase.controller;

import com.yukonproject.autobase.AutoBaseApplication;
import com.yukonproject.autobase.model.User;
import com.yukonproject.autobase.model.enums.Roles;
import com.yukonproject.autobase.service.UserService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.text.ParseException;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AutoBaseApplication.class)
@ActiveProfiles("test")
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() throws ParseException {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

        User user = new User();
        user.setRole(Roles.DRIVER);
        user.setFullName("Jack");
        user.setPhone("+38 (050) 450-33-21");
        user.setEmail("jack@gmail.com");
        userService.create(user);
    }

    @Test
    public void verifyAllUsersList() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/users")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].fullName").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].phone").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].email").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("[*].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("[*].fullName").value("Jack"))
                .andExpect(MockMvcResultMatchers.jsonPath("[*].phone").value("+38 (050) 450-33-21"))
                .andExpect(MockMvcResultMatchers.jsonPath("[*].email").value("jack@gmail.com"));
    }

    @Test
    public void verifyNotFoundUsers() throws Exception {
        userService.delete(1);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/users")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void createUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"role\":\"CLIENT\", \"fullName\":\"Mark\", \"phone\":\"+38 (050) 450-33-21\", \"email\":\"mark@gmail.com\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

    }

    @Test
    public void getUserById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/user/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.fullName").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.phone").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.fullName").value("Jack"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.phone").value("+38 (050) 450-33-21"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("jack@gmail.com"));
    }

    @Test
    public void getUserByIdNotFound() throws Exception {
        userService.delete(1);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/user/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"role\":\"DRIVER\", \"fullName\":\"Tyler\", \"phone\":\"+38 (095) 440-17-32\", \"email\":\"tyler@gmail.com\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateUserNotModified() throws Exception {
        userService.delete(1);
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"role\":\"DRIVER\", \"fullName\":\"Tyler\", \"phone\":\"+38 (095) 440-17-32\", \"email\":\"tyler@gmail.com\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotModified());
    }

    @Test
    public void deleteUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/{id}", 1))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteUserNotModified() throws Exception {
        userService.delete(1);
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/{id}", 1))
                .andExpect(status().isNotModified());
    }
}
