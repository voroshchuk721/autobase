package com.yukonproject.autobase.dto;

import com.yukonproject.autobase.model.User;
import com.yukonproject.autobase.model.Vehicle;
import com.yukonproject.autobase.model.enums.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderResponse {
    private Long id;
    private String departurePlace;
    private String arrivalPlace;
    private int passengerAmount;
    private Date date;
    private OrderStatus status;
    private User user;
    private Vehicle vehicle;
}
