package com.yukonproject.autobase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TripRequest {
    Long orderId;
    boolean orderStatus;
    boolean vehicleCondition;
}
