package com.yukonproject.autobase.dto;

import com.yukonproject.autobase.model.enums.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequest {
    private String departurePlace;
    private String arrivalPlace;
    private int passengerAmount;
    private Long userId;
    private Long vehicleId;
}
