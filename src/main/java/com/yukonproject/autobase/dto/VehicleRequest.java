package com.yukonproject.autobase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class VehicleRequest {

    private String model;
    private String color;
    private int passengerAmount;

}
