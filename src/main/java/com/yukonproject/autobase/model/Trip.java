package com.yukonproject.autobase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yukonproject.autobase.model.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@Table(name = "trips")
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "departure_place")
    private String departurePlace;

    @Column(name = "arrival_place")
    private String arrivalPlace;

    @Column(name = "passenger_amount")
    private int passengerAmount;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToOne(mappedBy = "trip")
    @JsonIgnore
    private Order order;

    public Trip(String departurePlace, String arrivalPlace, int passengerAmount) {
        this.departurePlace = departurePlace;
        this.arrivalPlace = arrivalPlace;
        this.passengerAmount = passengerAmount;
        this.status = Status.IN_PROCESS;
    }

    public Trip() {
        this.status = Status.IN_PROCESS;
    }
}
