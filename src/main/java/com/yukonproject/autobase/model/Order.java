package com.yukonproject.autobase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yukonproject.autobase.model.enums.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "price")
    private double price;

    @Column(name = "date")
    private Date date;

    @Column(length = 64, name = "status")
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonIgnore
    private User user;

    @ManyToOne
    @JoinColumn(name = "vehicle_id", referencedColumnName = "id")
    @JsonIgnore
    private Vehicle vehicle;

    @OneToOne
    @JoinColumn(name = "trip_id", referencedColumnName = "id")
    @JsonIgnore
    private Trip trip;

    public Order(double price, User user, Vehicle vehicle, Trip trip) {
        this.price = price;
        this.date = new Date();
        this.status = OrderStatus.IN_PROCESS;
        this.user = user;
        this.vehicle = vehicle;
        this.trip = trip;
    }

    public Order() {
        this.date = new Date();
        this.status = OrderStatus.IN_PROCESS;
    }
}
