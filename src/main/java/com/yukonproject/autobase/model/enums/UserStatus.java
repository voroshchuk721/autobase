package com.yukonproject.autobase.model.enums;

public enum UserStatus {
    ACTIVE,
    BANNED
}
