package com.yukonproject.autobase.model.enums;

public enum OrderStatus {
    IN_PROCESS,
    DONE
}
