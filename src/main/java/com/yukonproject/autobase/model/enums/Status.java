package com.yukonproject.autobase.model.enums;

public enum Status {
    IN_PROCESS,
    DONE,
}
