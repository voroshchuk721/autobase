package com.yukonproject.autobase.model.enums;

public enum Condition {
    READY,
    BUSY,
    BROKEN;
}
