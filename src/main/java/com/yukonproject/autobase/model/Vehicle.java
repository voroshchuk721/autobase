package com.yukonproject.autobase.model;

import com.yukonproject.autobase.model.enums.Condition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@AllArgsConstructor
@Table(name = "vehicles")
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 64, name = "model")
    private String model;

    @Column(length = 64, name = "plate_number")
    private String plateNumber;

    @Column(length = 64, name = "condition")
    private Condition condition;

    @Column(length = 64, name = "color")
    private String color;

    @Column(name = "passenger_amount")
    private Integer passengerAmount;

    public Vehicle(String model, String plateNumber, Integer passengerAmount, String color) {
        this.model = model;
        this.plateNumber = plateNumber;
        this.passengerAmount = passengerAmount;
        this.color = color;
        this.condition = Condition.READY;
    }

    public Vehicle() {
        this.condition = Condition.READY;
    }

}

//@OneToOne
//    @JoinColumn(name = "user_id")
//    private User user;




