package com.yukonproject.autobase.service;

import com.yukonproject.autobase.dto.OrderRequest;
import com.yukonproject.autobase.dto.OrderResponse;
import com.yukonproject.autobase.model.Order;

import java.util.List;

public interface OrderService {

    void create(OrderRequest orderRequest);

    List<OrderResponse> readAll();

    //GET USER ORDERS
    List<OrderResponse> readAllByUser(Long id);

    OrderResponse read(long id);

    boolean update(Order vehicle, long id);

    boolean delete(long id);

}
