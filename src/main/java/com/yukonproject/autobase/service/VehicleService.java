package com.yukonproject.autobase.service;

import com.yukonproject.autobase.model.Vehicle;


import java.util.List;

public interface VehicleService {

    void create(Vehicle vehicle);

    List<Vehicle> readAll();

    Vehicle read(long id);

    boolean update(Vehicle vehicle, long id);

    boolean delete(long id);

    void statusChange(long key);

}
