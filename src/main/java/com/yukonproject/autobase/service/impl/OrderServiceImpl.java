package com.yukonproject.autobase.service.impl;

import com.yukonproject.autobase.dto.OrderRequest;
import com.yukonproject.autobase.dto.OrderResponse;
import com.yukonproject.autobase.model.Order;
import com.yukonproject.autobase.model.Trip;
import com.yukonproject.autobase.model.User;
import com.yukonproject.autobase.model.Vehicle;
import com.yukonproject.autobase.model.enums.Condition;
import com.yukonproject.autobase.repository.OrderRepository;
import com.yukonproject.autobase.repository.TripRepository;
import com.yukonproject.autobase.repository.UserRepository;
import com.yukonproject.autobase.repository.VehicleRepository;
import com.yukonproject.autobase.service.OrderService;
import com.yukonproject.autobase.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final VehicleRepository vehicleRepository;
    private final TripRepository tripRepository;
    private final VehicleService vehicleService;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, UserRepository userRepository, VehicleRepository vehicleRepository, TripRepository tripRepository, VehicleService vehicleService) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.vehicleRepository = vehicleRepository;
        this.tripRepository = tripRepository;
        this.vehicleService = vehicleService;
    }


    @Override
    public void create(OrderRequest orderRequest) {

        Trip trip = new Trip(
                orderRequest.getDeparturePlace(),
                orderRequest.getArrivalPlace (),
                orderRequest.getPassengerAmount()
        );
        tripRepository.save(trip);
        User user = userRepository.findById(orderRequest.getUserId()).get();
        Vehicle vehicle = vehicleRepository.findById(orderRequest.getVehicleId()).get();
        //change condition to BUSY
        //getOne
        vehicle.setCondition(Condition.BUSY);
        vehicleService.update(vehicle, vehicle.getId());
        //getting just created trip
        trip = tripRepository.findTopByOrderByIdDesc();
        Order order = new Order(
                0,
                user,
                vehicle,
                trip
        );
        orderRepository.save(order);
    }


    @Override
    public List<OrderResponse> readAll() {
        List<Order> orders = orderRepository.findAll();
        OrderResponse orderResponse;
        List<OrderResponse> ordersResponse = new ArrayList<>();
        Trip trip;
        for (Order order: orders) {
            trip = order.getTrip();
            // trip = tripRepository.getOne(order.getId());
            orderResponse = new OrderResponse(
                    order.getId(),
                    trip.getDeparturePlace(),
                    trip.getArrivalPlace(),
                    trip.getPassengerAmount(),
                    order.getDate(),
                    order.getStatus(),
                    order.getUser(),
                    order.getVehicle()
            );
            ordersResponse.add(orderResponse);
        }
        return ordersResponse;
    }

    //GET USER ORDERS
    @Override
    public List<OrderResponse> readAllByUser(Long id) {
        User user = userRepository.findById(id).get();
        List<Order> orders = orderRepository.findAllByUser(user);
        OrderResponse orderResponse;
        List<OrderResponse> ordersResponse = new ArrayList<>();
        Trip trip;
        for (Order order: orders) {
            trip = tripRepository.getOne(order.getId());
            orderResponse = new OrderResponse(
                    order.getId(),
                    trip.getDeparturePlace(),
                    trip.getArrivalPlace(),
                    trip.getPassengerAmount(),
                    order.getDate(),
                    order.getStatus(),
                    order.getUser(),
                    order.getVehicle()
            );
            ordersResponse.add(orderResponse);
        }
        return ordersResponse;
    }

    @Override
    public OrderResponse read(long id) {
        Order order = orderRepository.getOne(id);
        Trip trip = order.getTrip();
//        Trip trip = tripRepository.getOne(order.getId());
        OrderResponse orderResponse = new OrderResponse(
                order.getId(),
                trip.getDeparturePlace(),
                trip.getArrivalPlace(),
                trip.getPassengerAmount(),
                order.getDate(),
                order.getStatus(),
                order.getUser(),
                order.getVehicle()
        );
        return orderResponse;
    }

    @Override
    public boolean update(Order order, long id) {
        if (orderRepository.existsById(id)) {
            order.setId(id);
            orderRepository.save(order);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(long id) {
        if (orderRepository.existsById(id)) {
            orderRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
