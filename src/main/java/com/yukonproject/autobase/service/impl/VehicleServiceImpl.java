package com.yukonproject.autobase.service.impl;

import com.yukonproject.autobase.model.Vehicle;
import com.yukonproject.autobase.model.enums.Condition;
import com.yukonproject.autobase.repository.VehicleRepository;
import com.yukonproject.autobase.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;
    private Condition condition;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }


    @Override
    public void create(Vehicle vehicle) {
        vehicleRepository.save(vehicle);
    }

    @Override
    public List<Vehicle> readAll() {
        return vehicleRepository.findAll();
    }

    @Override
    public Vehicle read(long id) {
        if (vehicleRepository.existsById(id))
            return vehicleRepository.findById(id).get();
        else return null;
    }

    @Override
    public boolean update(Vehicle vehicleOrig, long id) {
        if (vehicleRepository.existsById(id)) {
            Optional<Vehicle> found = vehicleRepository.findById(id);
            if (found.isPresent()) {
                Vehicle vehicle = found.get();
                vehicle.setId(id);
                if (vehicleOrig.getModel() == null) {
                    vehicle.setModel(vehicleOrig.getModel());
                }
                if (vehicleOrig.getPlateNumber() == null) {
                    vehicle.setPlateNumber(vehicleOrig.getPlateNumber());
                }
                if (vehicleOrig.getCondition() == null) {
                    vehicle.setCondition(vehicleOrig.getCondition());
                }
                if (vehicleOrig.getColor() == null) {
                    vehicle.setColor(vehicleOrig.getColor());
                }
                if (vehicleOrig.getPassengerAmount() == null) {
                    vehicle.setPassengerAmount(vehicleOrig.getPassengerAmount());
                }
                vehicleRepository.save(vehicle);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean delete(long id) {
        if (vehicleRepository.existsById(id)) {
            vehicleRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public void statusChange(long key){
        if(key==1){
            condition = Condition.BUSY;
        }else condition = Condition.READY;
    }
}
