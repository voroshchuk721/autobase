package com.yukonproject.autobase.service.impl;

import com.yukonproject.autobase.dto.TripRequest;
import com.yukonproject.autobase.model.Order;
import com.yukonproject.autobase.model.Trip;
import com.yukonproject.autobase.model.Vehicle;
import com.yukonproject.autobase.model.enums.Condition;
import com.yukonproject.autobase.model.enums.OrderStatus;
import com.yukonproject.autobase.model.enums.Status;
import com.yukonproject.autobase.repository.OrderRepository;
import com.yukonproject.autobase.repository.TripRepository;
import com.yukonproject.autobase.repository.VehicleRepository;
import com.yukonproject.autobase.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TripServiceImpl implements TripService {

    private final TripRepository tripRepository;
    private final OrderRepository orderRepository;
    private final VehicleRepository vehicleRepository;

    @Autowired
    public TripServiceImpl(TripRepository tripRepository, OrderRepository orderRepository, VehicleRepository vehicleRepository) {
        this.tripRepository = tripRepository;
        this.orderRepository = orderRepository;
        this.vehicleRepository = vehicleRepository;
    }


    @Override
    public void create(Trip trip) {
        tripRepository.save(trip);
    }

    //CLOSE ORDER
    @Override
    public void closeOrder(TripRequest tripRequest) {
        Order order = orderRepository.findById(tripRequest.getOrderId()).get();
        order.setStatus(OrderStatus.DONE);
        Trip trip = tripRepository.findById(order.getTrip().getId()).get();
        trip.setStatus(Status.DONE);
        Vehicle vehicle = vehicleRepository.findById(order.getVehicle().getId()).get();
        if (tripRequest.isVehicleCondition()) {
            vehicle.setCondition(Condition.BROKEN);
        }
        else {
            vehicle.setCondition(Condition.READY);
        }
        vehicleRepository.save(vehicle);
        tripRepository.save(trip);
        orderRepository.save(order);
    }

    @Override
    public List<Trip> readAll() {
        return tripRepository.findAll();
    }

    @Override
    public Trip read(long id) {
        return tripRepository.getOne(id);
    }

    @Override
    public boolean update(Trip trip, long id) {
        if (tripRepository.existsById(id)) {
            trip.setId(id);
            tripRepository.save(trip);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(long id) {
        if (tripRepository.existsById(id)) {
            tripRepository.deleteById(id);
            return true;
        }
        return false;
    }


}

