package com.yukonproject.autobase.service;

import com.yukonproject.autobase.dto.TripRequest;
import com.yukonproject.autobase.model.Trip;

import java.util.List;

public interface TripService {

    void create(Trip trip);

    //CLOSE ORDER
    void closeOrder(TripRequest tripRequest);

    List<Trip> readAll();

    Trip read(long id);

    boolean update(Trip trip, long id);

    boolean delete(long id);
}

