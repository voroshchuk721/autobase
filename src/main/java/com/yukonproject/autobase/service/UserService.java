package com.yukonproject.autobase.service;

import com.yukonproject.autobase.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService  {

    void create(User client);

    List<User> readAll();

    User read(long id);

    boolean update(User client, long id);

    boolean delete(long id);


}
