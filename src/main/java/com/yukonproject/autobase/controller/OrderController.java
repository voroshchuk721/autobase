package com.yukonproject.autobase.controller;

import com.yukonproject.autobase.dto.OrderRequest;
import com.yukonproject.autobase.dto.OrderResponse;
import com.yukonproject.autobase.model.Order;
import com.yukonproject.autobase.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping(value = "/order")
    public ResponseEntity<?> create(@RequestBody OrderRequest orderRequest) {
        orderService.create(orderRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/order")
    public ResponseEntity<List<OrderResponse>> read() {
        final List<OrderResponse> orders = orderService.readAll();

        return orders != null && !orders.isEmpty()
                ? new ResponseEntity(orders, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/order/{id}")
    public ResponseEntity<OrderResponse> read(@PathVariable(name = "id") long id) {
        final OrderResponse order = orderService.read(id);

        return order != null
                ? new ResponseEntity<>(order, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @PutMapping(value = "/order/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") long id, @RequestBody Order order) {
        final boolean updated = orderService.update(order, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/order/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") long id) {
        final boolean deleted = orderService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
}
