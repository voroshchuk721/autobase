package com.yukonproject.autobase.controller;

import com.yukonproject.autobase.model.Vehicle;
import com.yukonproject.autobase.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class VehicleController {

    private final VehicleService vehicleService;

    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @PostMapping(value = "/vehicle")
    public ResponseEntity<?> create(@RequestBody Vehicle vehicle) {
        vehicleService.create(vehicle);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/vehicle")
    public ResponseEntity<List<Vehicle>> read() {
        final List<Vehicle> vehicles = vehicleService.readAll();

        return vehicles != null && !vehicles.isEmpty()
                ? new ResponseEntity<>(vehicles, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/vehicle/{id}")
    public ResponseEntity<Vehicle> read(@PathVariable(name = "id") long id) {
        final Vehicle vehicle = vehicleService.read(id);

        return vehicle != null
                ? new ResponseEntity<>(vehicle, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @PutMapping(value = "/vehicle/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") long id, @RequestBody Vehicle vehicle) {
        final boolean updated = vehicleService.update(vehicle, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/vehicle/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") long id) {
        final boolean deleted = vehicleService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
}
