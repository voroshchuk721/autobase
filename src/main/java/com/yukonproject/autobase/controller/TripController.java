package com.yukonproject.autobase.controller;

import com.yukonproject.autobase.dto.TripRequest;
import com.yukonproject.autobase.model.Trip;
import com.yukonproject.autobase.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class TripController {

    private final TripService tripService;

    @Autowired
    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @PostMapping(value = "/trip")
    public ResponseEntity<?> create(@RequestBody Trip trip) {
        tripService.create(trip);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    //CLOSE ORDER
    @PostMapping(value = "/closeTrip")
    public ResponseEntity<?> closeOrder(@RequestBody TripRequest tripRequest) {
        tripService.closeOrder(tripRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/trip")
    public ResponseEntity<List<Trip>> read() {
        final List<Trip> trips = tripService.readAll();

        return trips != null && !trips.isEmpty()
                ? new ResponseEntity<>(trips, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/trip/{id}")
    public ResponseEntity<Trip> read(@PathVariable(name = "id") long id) {
        final Trip trip = tripService.read(id);

        return trip != null
                ? new ResponseEntity<>(trip, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @PutMapping(value = "/trip/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") long id, @RequestBody Trip trip) {
        final boolean updated = tripService.update(trip, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/trip/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") long id) {
        final boolean deleted = tripService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
}
