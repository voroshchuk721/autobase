package com.yukonproject.autobase.repository;

import com.yukonproject.autobase.model.Trip;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TripRepository extends JpaRepository<Trip, Long> {
    Trip findTopByOrderByIdDesc();
}
