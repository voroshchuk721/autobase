package com.yukonproject.autobase.repository;

import com.yukonproject.autobase.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;


public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    Vehicle findByModel(String model);
}
