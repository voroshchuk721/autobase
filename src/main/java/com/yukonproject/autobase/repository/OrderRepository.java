package com.yukonproject.autobase.repository;

import com.yukonproject.autobase.dto.OrderResponse;
import com.yukonproject.autobase.model.Order;
import com.yukonproject.autobase.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface OrderRepository extends JpaRepository<Order, Long> {
    //GET USER ORDERS
    List<Order> findAllByUser(User user);
}
