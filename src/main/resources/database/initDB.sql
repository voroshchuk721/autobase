CREATE TABLE IF NOT EXISTS vehicles(
    id BIGSERIAL PRIMARY KEY,
    model varchar(64) NOT NULL,
    plate_number varchar(64),
    condition varchar(64),
    color varchar(64) NOT NULL,
    passenger_amount integer NOT NULL
);

CREATE TABLE IF NOT EXISTS users(
    id BIGSERIAL PRIMARY KEY,
    role varchar(64) NOT NULL,
    full_name varchar(128) NOT NULL,
    phone varchar(64) NOT NULL,
    email varchar(128),
    balance double precision NULL,
    password varchar(64) NOT NULL,
    status varchar(64)
);

CREATE TABLE IF NOT EXISTS orders(
                                     id BIGSERIAL PRIMARY KEY,
                                     price double precision NOT NULL,
                                     date date NOT NULL,
                                     status varchar(64) NOT NULl,
    user_id bigint,
    CONSTRAINT FK_UserOrder FOREIGN KEY (user_id)
    REFERENCES users(id),
    vehicle_id bigint,
    CONSTRAINT FK_VehicleOrder FOREIGN KEY(vehicle_id)
    REFERENCES vehicles(id),
    trip_id bigint,
    CONSTRAINT FK_TripOrder FOREIGN KEY (trip_id)
    REFERENCES trips(id)
);

CREATE TABLE IF NOT EXISTS trips(
                                    id BIGSERIAL PRIMARY KEY,
                                    departure_place varchar(128),
    arrival_place varchar(128),
    passenger_amount INTEGER,
    status varchar(64)
    );


