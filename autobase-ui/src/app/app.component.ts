import { Component } from '@angular/core';
import { AutobaseService } from './services/autobase.service';
import { HttpErrorResponse } from "@angular/common/http";
import { NgForm } from "@angular/forms";
import {Router} from '@angular/router';
import {AuthService} from './core/service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'autobase-ui';

  // @ts-ignore
  public user;
  // @ts-ignore
  public vehicles;

  public statusValue: boolean = false;
  public conditionValue: boolean = false;

  constructor(private autobaseService: AutobaseService,
              private router: Router,
              private authService: AuthService) { }

  public async onLogout(): Promise<void> {
    await this.authService.logOut().toPromise()
    this.router.navigate(['/'])
  }

  onAddUser(addForm: NgForm) {
    // @ts-ignore
    document.getElementById('add-user-form').click();
    this.autobaseService.addUser(addForm.value).subscribe(
      data => {
        console.log(data);
        //очистка форми
        addForm.reset();
        this.router.navigate(['/user'])
      },
      (error: HttpErrorResponse) => {
        //відображення помилки
        console.log(error.message)
        //очистка форми
        addForm.reset();
      }
    );
  }

  onAddVehicle(addForm: NgForm) {
    // @ts-ignore
    document.getElementById('add-vehicle-form').click();
    this.autobaseService.addVehicle(addForm.value).subscribe(
      data => {
        console.log(data);
        //очистка форми
        addForm.reset();
        this.router.navigate(['/vehicle'])
      },
      (error: HttpErrorResponse) => {
        //відображення помилки
        console.log((error.message))
        //очистка форми
        addForm.reset();
      }
    );
  }

  onAddOrder(addForm: NgForm) {
    console.log(addForm.value)
    const userId = parseInt(<string> localStorage.getItem('userId'), 10)
    const body = {
      ...addForm.value,
      userId
    }
    // @ts-ignore
    document.getElementById('add-order-form').click();
    this.autobaseService.addOrder(body).subscribe(
      data => {
        console.log(data);
        //очистка форми
        addForm.reset();
        this.router.navigate(['/order'])
      },
      (error: HttpErrorResponse) => {
        //відображення помилки
        console.log((error.message))
        //очистка форми
        addForm.reset();
      }
    );
  }

  //CLOSE ORDER
  onOrderClose(addForm: NgForm) {
    // @ts-ignore
    document.getElementById('close-order-form').click();
    this.autobaseService.closeOrder(addForm.value).subscribe(
      data => {
        console.log(data);
        //очистка форми
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        //відображення помилки
        console.log((error.message))
        //очистка форми
        addForm.reset();
        this.router.navigate(['/order'])
      }
    );
  }

  getVehicles() {
    this.autobaseService.getAllVehicles().subscribe(
      data => { this.vehicles = data; },
      (error: HttpErrorResponse) => {
        console.log((error.message))
      }
    );
  }

  public onOpenModal(mode: string): void {
    const container = document.getElementById('navbarColor02');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'addUser') {
      button.setAttribute('data-target', '#addUserModal');
    }
    if (mode === 'addVehicle') {
      button.setAttribute('data-target', '#addVehicleModal');
    }
    if (mode === 'addOrder') {
      button.setAttribute('data-target', '#addOrderModal');
      this.getVehicles();
    }
    if (mode === 'orderClose') {
      button.setAttribute('data-target', '#orderCloseModal');
    }
    /*if (mode === 'delete') {
      this.deleteEmployee = employee;
      button.setAttribute('data-target', '#deleteUserModal');
    }*/
    // @ts-ignore
    container.appendChild(button);
    button.click();
  }

  public onTogleStatus(data: string) {
    this.statusValue = Boolean(data);
  }

  public onTogleCondition(data: string) {
    this.conditionValue = Boolean(data);
  }
}
