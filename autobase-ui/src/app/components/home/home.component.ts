import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {AuthService} from '../../core/service/auth.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public form = this.formBuilder.group({
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null, [Validators.required])
  })
  public error = ''

  constructor(private router: Router,
              private route: ActivatedRoute,
              private authService: AuthService,
              private formBuilder: FormBuilder) { }

  public ngOnInit(): void {
    this.route.queryParams.subscribe((params: Params) => {
      if (params['accessDenied']) {
        this.error = 'Авторизуйтесь в системі!'
      }
      else if (params['sessionFailed']) {
        this.error = 'Авторизуйтесь в системі!'
      }
    })
  }

  public onLogin() {
    this.authService.login(this.form.value)
      .subscribe(
        (res) => {
          console.log(res)
          this.error = ''
          this.router.navigate(['/user'])
        },
        e => this.error = e.error
        )
  }

}
