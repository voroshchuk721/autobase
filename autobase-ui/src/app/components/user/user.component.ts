import { Component, OnInit } from '@angular/core';
import { AutobaseService } from '../../services/autobase.service';
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  // @ts-ignore
  public users;

  constructor(private autobaseService: AutobaseService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this.autobaseService.getAllUsers().subscribe(
      data => { this.users = data; },
      (error: HttpErrorResponse) => {
        console.log((error.message))
      }
    );
  }

}
