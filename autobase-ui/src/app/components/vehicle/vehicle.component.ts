import { Component, OnInit } from '@angular/core';
import { AutobaseService } from '../../services/autobase.service';
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {

  // @ts-ignore
  public vehicles;

  constructor(private autobaseService: AutobaseService) { }

  ngOnInit(): void {
    this.getVehicles();
  }

  getVehicles() {
    this.autobaseService.getAllVehicles().subscribe(
      data => { this.vehicles = data; },
      (error: HttpErrorResponse) => {
        console.log((error.message))
      }
    );
  }

}
