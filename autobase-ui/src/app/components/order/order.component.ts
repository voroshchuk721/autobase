import { Component, OnInit } from '@angular/core';
import { AutobaseService } from '../../services/autobase.service';
import { HttpErrorResponse } from "@angular/common/http";
import {OrderInterface} from '../../core/interfaces/order.interface';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  // @ts-ignore
  public orders: OrderInterface[];

  constructor(private autobaseService: AutobaseService) { }

  ngOnInit(): void {
    this.getOrders();
  }

  getOrders() {
    this.autobaseService.getAllOrders().subscribe(
      data => { this.orders = data },
      (error: HttpErrorResponse) => { console.log(error.message) }
    );
  }
}
