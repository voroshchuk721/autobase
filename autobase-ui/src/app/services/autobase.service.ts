import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from "rxjs";
import {OrderInterface} from '../core/interfaces/order.interface';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class AutobaseService {

  constructor(private http: HttpClient) { }

  getAllUsers() {
    return this.http.get('/api/v1/users');
  }

  getUserById(id: number) {
    return this.http.get('/api/v1/user/' + id);
  }

  getUserByEmail(email: string, password: string) {
    return this.http.get('/api/v1/user/' + email + '/' + password);
  }

  addUser(user: any) {
    const body = JSON.stringify(user);
    console.log(body);
    return this.http.post('/api/v1/users', body, httpOptions);
  }

  getAllVehicles() {
    return this.http.get('/api/v1/vehicle');
  }

  getVehicleById(id: number) {
    return this.http.get('/api/v1/vehicle/' + id);
  }

  addVehicle(vehicle: any) {
    const body = JSON.stringify(vehicle);
    console.log(body);
    return this.http.post('/api/v1/vehicle/', body, httpOptions);
  }

  getAllOrders(): Observable<OrderInterface[]> {
    return this.http.get<OrderInterface[]>('/api/v1/order');
  }

  getUserOrders(id: number) {
    return this.http.get('/api/v1/user/' + id + '/orders');
  }

  addOrder(order: any) {
    const body = JSON.stringify(order);
    console.log(body);
    return this.http.post('/api/v1/order/', body, httpOptions);
  }

  closeOrder(order: any) {
    const body = JSON.stringify(order);
    console.log(body);
    return this.http.post('/api/v1/closeTrip/', body, httpOptions);
  }
}
