import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component'
import { UserComponent } from './components/user/user.component'
import { VehicleComponent } from './components/vehicle/vehicle.component'
import { OrderComponent } from './components/order/order.component'

const routes: Routes = [
  {
    path:'',
    component: HomeComponent
  },
  {
    path:'user',
    component: UserComponent
  },
  {
    path:'vehicle',
    component: VehicleComponent
  },
  {
    path:'order',
    component: OrderComponent
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
