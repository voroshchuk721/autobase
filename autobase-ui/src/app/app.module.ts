import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AutobaseService } from './services/autobase.service';
import { UserComponent } from './components/user/user.component';
import { VehicleComponent } from './components/vehicle/vehicle.component';
import { OrderComponent } from './components/order/order.component';
import {TokenInterceptor} from './core/interceptor/interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    VehicleComponent,
    OrderComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule
    ],
  providers: [AutobaseService,
    {
     provide: HTTP_INTERCEPTORS,
     multi: true,
     useClass: TokenInterceptor
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
