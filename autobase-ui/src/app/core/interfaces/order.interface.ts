export interface OrderInterface {
  id: number
  departurePlace: string
  arrivalPlace: string
  passengerAmount: string
  date: string
  status: string
  vehicle: any
  user: any
}
